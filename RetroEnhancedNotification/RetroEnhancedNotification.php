<?php

namespace RetroEnhancedNotification;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\InstallContext;

/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class RetroEnhancedNotification extends Plugin
{
    public function activate(ActivateContext $context){

    }

    public function update(UpdateContext $context){
        
    }
    
    public function uninstall(UninstallContext $context){
       
    }
    
    public function install(InstallContext $context){
           
    }
}
