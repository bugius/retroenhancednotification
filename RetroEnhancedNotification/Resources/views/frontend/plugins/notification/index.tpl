{extends file="parent:frontend/plugins/notification/index.tpl"}

{block name="frontend_detail_index_notification_privacy"}
	{* Captcha *}
	{block name="frontend_notify_form_captcha"}
	    {if !({config name=noCaptchaAfterLogin} && $sUserLoggedIn)}
	        {$notifyCaptchaName = {config name=captchaMethod}}
	        <div class="notify--captcha-form">
	            {if $notifyCaptchaName === 'legacy'}
	                <div class="notify--captcha">

	                    {* Deferred loading of the captcha image *}
	                    {block name='frontend_notify_form_captcha_placeholder'}
	                        <div class="captcha--placeholder" {if $sErrorFlag.sCaptcha}
	                             data-hasError="true"{/if}
	                             data-src="{url module=widgets controller=Captcha action=refreshCaptcha}"
	                             data-autoload="true">
	                        </div>
	                    {/block}

	                    {block name='frontend_notify_form_captcha_label'}
	                        <strong class="captcha--notice">{s name="SupportLabelCaptcha" namespace="frontend/forms/elements"}{/s}</strong>
	                    {/block}

	                    {block name='frontend_notify_form_captcha_code'}
	                        <div class="captcha--code">
	                            <input type="text" name="sCaptcha" class="notify--field{if $sErrorFlag.sCaptcha} has--error{/if}" required="required" aria-required="true" />
	                        </div>
	                    {/block}
	                </div>
	            {else}
	                {$captchaName = $notifyCaptchaName}
	                {$captchaHasError = isset($sErrorFlag) && count($sErrorFlag) > 0}
	                {include file="widgets/captcha/custom_captcha.tpl" captchaName=$captchaName captchaHasError=$captchaHasError}
	            {/if}
	        </div>
	    {/if}
	{/block}
	{$smarty.block.parent}
{/block}